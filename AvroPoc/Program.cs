﻿using System;

namespace AvroPoc
{
    public class AvroPoc
    {
        static string sectionDivider = "---------------------------------------------------------------------------------------- ";
        static void Main()
        {
            
            AvroWithObjectContainers avroWithObjectContainers = new AvroWithObjectContainers();

            // Write Data for SensorData
            string pathForSensorData = "AvroSenserData.avro";
            var testDataSensorData = new[]
            {
                new SensorData { Value = new byte[] { 1, 2, 3, 4, 5 }, Position = new Location { Room = 243, Floor = 1 } },
                new SensorData { Value = new byte[] { 6, 7, 8, 9 }, Position = new Location { Room = 244, Floor = 1 } }
            };
            avroWithObjectContainers.SerailizeAndWrite(testDataSensorData, pathForSensorData);

            // Write Data for NewSensorData
            string pathForNewSensorData = "AvroNewSenserData.avro";
            var testDataNewSensorData = new[]
            {
                new NewSensorData { Value = new byte[] { 1, 2, 3, 4, 5 }, Position = new Location { Room = 243, Floor = 1 }, SensorDeviceName = "1", SomeInt = 3},
                new NewSensorData { Value = new byte[] { 6, 7, 8, 9 }, Position = new Location { Room = 244, Floor = 1 }, SensorDeviceName = "2", SomeInt = 4}
            };
            avroWithObjectContainers.SerailizeAndWrite(testDataNewSensorData, pathForNewSensorData);

            // Case 1: Read with the same schema
            var actualSensorData = avroWithObjectContainers.ReadAndDeserialize<SensorData>(pathForSensorData);
            var actualNewSensorData = avroWithObjectContainers.ReadAndDeserialize<NewSensorData>(pathForNewSensorData);
            Console.WriteLine(sectionDivider);
            Console.WriteLine("SensorData schema for write and read");
            Console.WriteLine(ObjectDumper.Dump(actualSensorData));
            Console.WriteLine("NewSensorData schema for write and read");
            Console.WriteLine(ObjectDumper.Dump(actualNewSensorData));

            // Case 2: Read with a field removed in schema 
            var lesserData = avroWithObjectContainers.ReadAndDeserialize<SensorData>(pathForNewSensorData);
            Console.WriteLine(sectionDivider);
            Console.WriteLine("NewSensorData schema for write and SensorData schema for read");
            Console.WriteLine(ObjectDumper.Dump(lesserData));

            // Case 3: Read with additional field in schema
            var moreData = avroWithObjectContainers.ReadAndDeserialize<NewSensorData>(pathForSensorData);
            Console.WriteLine(sectionDivider);
            Console.WriteLine("SensorData schema for write and NewSensorData schema for read");
            Console.WriteLine(ObjectDumper.Dump(moreData));

            // Case 4: Read with a data type changed in schema - will blow up
            //var dataAfterTypeChange = avroWithObjectContainers.ReadAndDeserialize<NewSensorDataTypeChange>(pathForNewSensorData);
            //Console.WriteLine(sectionDivider);
            //Console.WriteLine("NewSensorData schema for write and SensorDataTypeChange schema for read");
            //Console.WriteLine(ObjectDumper.Dump(dataAfterTypeChange));

            // Case 5: Read with a variable name changed in schema
            var dataAfterNameChange = avroWithObjectContainers.ReadAndDeserialize<NewSensorDataNameChange>(pathForNewSensorData);
            Console.WriteLine(sectionDivider);
            Console.WriteLine("NewSensorData schema for write and SensorDataNameChange schema for read");
            Console.WriteLine(ObjectDumper.Dump(dataAfterNameChange));
            
            Console.WriteLine(sectionDivider);
            Console.WriteLine("Press any key to exit.");
            Console.Read();
        }
    }
}
