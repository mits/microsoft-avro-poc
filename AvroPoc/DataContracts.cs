﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace AvroPoc
{
    [DataContract(Name = "SensorDataValue", Namespace = "Sensors")]
    internal class SensorData
    {
        [DataMember(Name = "Location")]
        public Location Position;

        [DataMember(Name = "Value")]
        public byte[] Value;
    }

    //Sample struct used in serialization samples
    [DataContract]
    internal struct Location
    {
        [DataMember]
        public int Floor;

        [DataMember]
        public int Room;
    }

    [DataContract(Name = "SensorDataValue", Namespace = "Sensors")]
    internal class NewSensorData : SensorData
    {
        public NewSensorData()
        {
            this.SomeInt = 5;
        }

        [DataMember(Name = "SensorDeviceName")]
        public string SensorDeviceName;

        [DataMember(Name = "SomeInt")] 
        public int SomeInt { get; set; }
    }

    [DataContract(Name = "SensorDataValue", Namespace = "Sensors")]
    internal class NewSensorDataTypeChange : SensorData
    {
        [DataMember(Name = "SensorDeviceName")]
        public int SensorDeviceName;
    }

    [DataContract(Name = "SensorDataValue", Namespace = "Sensors")]
    internal class NewSensorDataNameChange
    {
        [DataMember(Name = "Location")]
        public Location Position;

        [DataMember(Name = "Value")]
        public byte[] Value;

        [DataMember(Name = "SensorDeviceName")]
        public string SensorDeviceNewName;
    }
}
