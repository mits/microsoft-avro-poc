﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using Microsoft.Hadoop.Avro.Container;
using Microsoft.Hadoop.Avro;
namespace AvroPoc
{
    public class AvroWithObjectContainers
    {
        public MemoryStream SerailizeAndWrite<T>(T[] data, string path)
        {
            using (var buffer = new MemoryStream())
            {
                Serialize(buffer, data);
                if (!WriteFile(buffer, path))
                {
                    Console.WriteLine("Error during file operation. Quitting method");
                }
                return buffer;
            }
        }

        public T[] ReadAndDeserialize<T>(string path)
        {
            using (var buffer = new MemoryStream())
            {
                if (!ReadFile(buffer, path))
                {
                    Console.WriteLine("Error during file operation. Quitting method");
                    throw new FileLoadException(path);
                }
                return Deseriazlie<T>(buffer);
            }
        }

        private void Serialize<T>(MemoryStream buffer, T[] data)
        {
            using (var w = AvroContainer.CreateWriter<T>(buffer, Codec.Deflate))
            {
                using (var writer = new SequentialWriter<T>(w, 24))
                {
                    foreach (var d in data)
                    {
                        writer.Write(d);
                    }
                }
            }
        }

        private T[] Deseriazlie<T>(MemoryStream buffer)
        {
            buffer.Seek(0, SeekOrigin.Begin);
            using (var reader = new SequentialReader<T>(
                AvroContainer.CreateReader<T>(buffer, true)))
            {
                
                return reader.Objects.ToArray();
            }
        }

        private dynamic Cast(dynamic obj, Type castTo)
        {
            return Convert.ChangeType(obj, castTo);
        }

        private bool WriteFile(MemoryStream InputStream, string path)
        {
            if (File.Exists(path))
            {
                RemoveFile(path);
            }
            try
            {
                using (FileStream fs = File.Create(path))
                {
                    InputStream.Seek(0, SeekOrigin.Begin);
                    InputStream.CopyTo(fs);
                }
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("The following exception was thrown during creation and writing to the file \"{0}\"", path);
                Console.WriteLine(e.Message);
                return false;
            }
        }

        private bool ReadFile(MemoryStream OutputStream, string path)
        {
            try
            {
                using (FileStream fs = File.Open(path, FileMode.Open))
                {
                    fs.CopyTo(OutputStream);
                }
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("The following exception was thrown during reading from the file \"{0}\"", path);
                Console.WriteLine(e.Message);
                return false;
            }
        }

        private void RemoveFile(string path)
        {
            if (File.Exists(path))
            {
                try
                {
                    File.Delete(path);
                }
                catch (Exception e)
                {
                    Console.WriteLine("The following exception was thrown during deleting the file \"{0}\"", path);
                    Console.WriteLine(e.Message);
                }
            }
            else
            {
                Console.WriteLine("Can not delete file \"{0}\". File does not exist", path);
            }
        }
    }
}
